import com.alibaba.fastjson.JSON;
import com.raycloud.sdk.shopee.DefaultShopeeClient;
import com.raycloud.sdk.shopee.exception.ApiException;
import com.raycloud.sdk.shopee.request.GetLogisticsRequest;
import com.raycloud.sdk.shopee.request.ItemAddRequest;
import com.raycloud.sdk.shopee.request.ItemGetCategoriesRequest;
import com.raycloud.sdk.shopee.response.GetLogisticsResponse;
import com.raycloud.sdk.shopee.response.ItemGetCategoriesResponse;
import org.junit.Test;

/**
 * Created by donar on 17/6/12.
 */
public class ItemTest {
    public DefaultShopeeClient client = new DefaultShopeeClient();
    @Test
    public void itemAdd() throws ApiException {
        GetLogisticsRequest request = new GetLogisticsRequest(24401648,10676,"afa34fd2c93404f20ed883da1013f6febf4a3e049d56546be6e76f3f3787e493");
        GetLogisticsResponse response = client.execute(request);
        System.out.println(JSON.toJSONString(response));
    }
}
