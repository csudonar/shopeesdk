package com.raycloud.sdk.shopee;

import com.raycloud.sdk.shopee.exception.ApiException;
import com.raycloud.sdk.shopee.request.ShopeeRequest;
import com.raycloud.sdk.shopee.response.ShopeeResponse;

/**
 * Created by donar on 17/6/9.
 */
public interface ShopeeClient {
    /**
     * @param <T>
     * @param request
     * @return
     * @throws ApiException
     */
    public <T extends ShopeeResponse> T execute(ShopeeRequest<T> request) throws ApiException;
   }
