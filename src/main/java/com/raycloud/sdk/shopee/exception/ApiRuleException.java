package com.raycloud.sdk.shopee.exception;

/**
 * Created by donar on 17/6/12.
 */
public class ApiRuleException extends ApiException{
    private static final long serialVersionUID = -7787145910600194272L;

    public ApiRuleException(String errCode, String errMsg) {
        super(errCode, errMsg);
    }
}
