package com.raycloud.sdk.shopee.util;

import java.util.HashMap;
public class RequestParametersHolder {
	private HashMap<String,String> protocalMustParams;
	private HashMap<String,String> applicationParams;

	public HashMap<String,String> getProtocalMustParams() {
		return protocalMustParams;
	}

	public void setProtocalMustParams(HashMap<String,String> protocalMustParams) {
		this.protocalMustParams = protocalMustParams;
	}

	public HashMap<String,String> getApplicationParams() {
		return applicationParams;
	}

	public void setApplicationParams(HashMap<String,String> applicationParams) {
		this.applicationParams = applicationParams;
	}

}
