package com.raycloud.sdk.shopee.util;


import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by donar on 17/6/12.
 */
public class ShopeeUtils {
    public static Long getUnixTime(){
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d = new Date();
            String t = df.format(d);
            Long epho =  df.parse(t).getTime() / 1000;
            System.out.println(epho);
            return epho;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0l;
    }
    public static String getSHA256Secret(String url,String jsonString,String secretKey){
        SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(),
                "HmacSHA256");
        Mac mac = null;
        try {
            mac = Mac.getInstance("HmacSHA256");

            mac.init(signingKey);
            return   byte2hex(mac.doFinal((url+"|"+jsonString).getBytes()));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        return null;


    }
    private static String byte2hex(byte[] bytes) {
        StringBuilder sign = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(bytes[i] & 0xFF);
            if (hex.length() == 1) {
                sign.append("0");
            }
            sign.append(hex.toUpperCase());
        }
        return sign.toString();
    }

    public static void main(String[] args) {
        System.out.println("\u5176\u4ed6");
    }
}
