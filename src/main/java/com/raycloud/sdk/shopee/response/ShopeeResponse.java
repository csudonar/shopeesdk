package com.raycloud.sdk.shopee.response;

import com.raycloud.sdk.shopee.constant.ApiField;
import java.io.Serializable;
import java.util.Map;

/**
 * TOPAPI基础响应信息。
 */
public abstract class ShopeeResponse implements Serializable {

	private static final long serialVersionUID = 5014379068811962022L;

	private String body;
	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
		parseBody(body);
	}
	public abstract void parseBody(String body);
}
