package com.raycloud.sdk.shopee.response;

import com.alibaba.fastjson.JSONObject;

import java.util.Date;

/**
 * Created by donar on 17/6/12.
 */
public class ItemUpdateStockResponse extends ShopeeResponse{
    Long itemId;
    Date modifiedTime;
    public void parseBody(String body) {
        JSONObject obj = JSONObject.parseObject("body").getJSONObject("item");
        setItemId(obj.getLong("item_id"));
        setModifiedTime(new Date(obj.getLong("modified_time")));
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
