package com.raycloud.sdk.shopee.response;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.raycloud.sdk.shopee.domain.Category;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by donar on 17/6/12.
 */
public class ItemGetCategoriesResponse extends ShopeeResponse{
    List<Category> categorys = new ArrayList<Category>();
    public void parseBody(String body) {
        JSONArray array  = JSON.parseObject(body).getJSONArray("categories");
        Iterator<Object> it = array.iterator();
        while (it.hasNext()){
            JSONObject jo = (JSONObject) it.next();
            Category category = new Category();
            category.setCategoryId(jo.getInteger("category_id"));
            category.setParentId(jo.getInteger("parent_id"));
            category.setCategoryName(jo.getString("category_name"));
            category.setHasChildren(jo.getBoolean("has_children"));
            categorys.add(category);
        }
    }

    public List<Category> getCategorys() {
        return categorys;
    }
}
