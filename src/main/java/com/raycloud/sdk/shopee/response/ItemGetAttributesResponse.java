package com.raycloud.sdk.shopee.response;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.raycloud.sdk.shopee.domain.Attribute;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/**
 * Created by donar on 17/6/12.
 */
public class ItemGetAttributesResponse extends ShopeeResponse{
    List<Attribute> attributes = new ArrayList<Attribute>();
    public void parseBody(String body) {
        JSONArray array = JSONObject.parseObject(body).getJSONArray("attributes");
        if(array==null)return;
        Iterator<Object> it = array.iterator();
        while (it.hasNext()){
            JSONObject jo = (JSONObject) it.next();
            Attribute attribute = new Attribute();
            attribute.setAttributeId(jo.getLong("attribute_id"));
            attribute.setAttributeName(jo.getString("attribute_name"));
            attribute.setMandatory(jo.getBoolean("is_mandatory"));
            attribute.setAttributeType(jo.getString("attribute_type"));
            attribute.setInputType(jo.getString("input_type"));
            attribute.setOptions(JSONArray.parseArray(jo.getString("options"),String.class));
            attributes.add(attribute);
        }
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

}
