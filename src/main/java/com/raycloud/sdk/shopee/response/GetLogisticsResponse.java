package com.raycloud.sdk.shopee.response;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.raycloud.sdk.shopee.domain.Logistic;
import com.raycloud.sdk.shopee.domain.Size;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by donar on 17/6/12.
 */
public class GetLogisticsResponse extends ShopeeResponse{
    List<Logistic>  logistics = new ArrayList<Logistic>();
    public void parseBody(String body) {
        JSONArray array = JSONObject.parseObject(body).getJSONArray("logistics");
        Iterator<Object> it = array.iterator();
        while (it.hasNext()){
            JSONObject jo = (JSONObject) it.next();
            Logistic logistic = new Logistic();
            logistic.setLogisticId(jo.getString("logistic_id"));
            logistic.setLogisticName(jo.getString("logistic_name"));
            logistic.setHasCod(jo.getBoolean("has_cod"));
            logistic.setEnabled(jo.getBoolean("enabled"));
            logistic.setFeeType(jo.getString("fee_type"));
            JSONArray sarray = jo.getJSONArray("sizes");
            if(sarray!=null){
                List<Size> sizes = new ArrayList<Size>();
                Iterator<Object> it2= sarray.iterator();
                while(it2.hasNext()){
                    JSONObject so = (JSONObject) it.next();
                    Size size = new Size();
                    size.setSizeId(so.getInteger("size_id"));
                    size.setName(so.getString("name"));
                    size.setDefaultPrice(so.getInteger("default_price"));
                    sizes.add(size);
                }
                logistic.setSizes(sizes);
            }
            logistics.add(logistic);
        }
    }

    public List<Logistic> getLogistics() {
        return logistics;
    }

}
