package com.raycloud.sdk.shopee.response;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.raycloud.sdk.shopee.domain.Item;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by donar on 17/6/12.
 */
public class ItemGetItemsListResponse extends ShopeeResponse {
    List<Item> items = new ArrayList<Item>();
    Boolean hasMore;
    public void parseBody(String body) {
        JSONObject obj = JSON.parseObject(body);
        setHasMore(obj.getBoolean("more"));
        JSONArray array = obj.getJSONArray("items");
        if(array!=null){
            Iterator<Object> it = array.iterator();
            while (it.hasNext()){
                JSONObject jo = (JSONObject) it.next();
                Item item = new Item();
                item.setItemId(jo.getLong("item_id"));
                item.setShopId(jo.getInteger("shopid"));
                item.setStatus(jo.getString("status"));
                items.add(item);
            }
        }
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Boolean getHasMore() {
        return hasMore;
    }

    public void setHasMore(Boolean hasMore) {
        this.hasMore = hasMore;
    }
}
