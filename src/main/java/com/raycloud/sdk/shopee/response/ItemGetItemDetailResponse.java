package com.raycloud.sdk.shopee.response;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.raycloud.sdk.shopee.domain.Item;
import com.raycloud.sdk.shopee.domain.Variation;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by donar on 17/6/12.
 */
public class ItemGetItemDetailResponse extends ShopeeResponse{
    private Item item;
    public void parseBody(String body) {
        JSONObject o = JSON.parseObject(body).getJSONObject("item");
        if(o!=null){
            item = new Item();
            item.setItemId(o.getLong("item_id"));
            item.setShopId(o.getInteger("shopid"));
            item.setItemSku(o.getString("item_sku"));
            item.setStatus(o.getString("status"));
            item.setName(o.getString("name"));
            item.setDescription(o.getString("description"));
            item.setImages(o.getString("images"));
            item.setCurrency(o.getString("currency"));
            item.setHasVariation(o.getBoolean("has_variation"));
            item.setPrice(o.getFloat("price"));
            item.setStock(o.getInteger("stock"));
            item.setCreateTime(new Date(o.getLong("create_time")));
            item.setUpdateTime(new Date(o.getLong("update_time")));
            if(item.getHasVariation()!=null&&item.getHasVariation()){
                JSONArray vas = o.getJSONArray("variations");
                List<Variation> list = new ArrayList<Variation>();
                Iterator<Object> it = vas.iterator();
                while(it.hasNext()){
                    JSONObject jo = (JSONObject) it.next();
                    Variation variation = new Variation();
                    variation.setVariationId(jo.getLong("variation_id"));
                    variation.setVariationSku(jo.getString("variation_sku"));
                    variation.setName(jo.getString("name"));
                    variation.setPrice(jo.getFloat("price"));
                    variation.setStock(jo.getInteger("stock"));
                    variation.setStatus(jo.getString("status"));
                    variation.setCreateTime(new Date(jo.getLong("create_time")));
                    variation.setUpdateTime(new Date(jo.getLong("update_time")));
                    list.add(variation);
                }
                item.setVariations(list);
            }
        }
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
