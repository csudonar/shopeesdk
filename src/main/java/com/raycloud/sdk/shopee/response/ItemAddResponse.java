package com.raycloud.sdk.shopee.response;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by donar on 17/6/12.
 */
public class ItemAddResponse extends ShopeeResponse {
    private Long itemId;
    private String warning;
    public Long getItemId() {
        return itemId;
    }
    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }
    public String getWarning() {
        return warning;
    }
    public void setWarning(String warning) {
        this.warning = warning;
    }
    public void parseBody(String body) {
        JSONObject jsonObject = JSONObject.parseObject(body);
        setItemId(jsonObject.getLong("item_id"));
        setWarning(jsonObject.getString("warning"));
    }
}
