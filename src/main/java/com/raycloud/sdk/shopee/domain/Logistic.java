package com.raycloud.sdk.shopee.domain;

import java.util.List;

/**
 * Created by donar on 17/6/12.
 */
public class Logistic {
    private String logisticId;
    private String logisticName;
    private Boolean hasCod;
    private Boolean enabled;
    private String feeType;
    private List<Size> sizes;

    public String getLogisticId() {
        return logisticId;
    }

    public void setLogisticId(String logisticId) {
        this.logisticId = logisticId;
    }

    public String getLogisticName() {
        return logisticName;
    }

    public void setLogisticName(String logisticName) {
        this.logisticName = logisticName;
    }

    public Boolean getHasCod() {
        return hasCod;
    }

    public void setHasCod(Boolean hasCod) {
        this.hasCod = hasCod;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public List<Size> getSizes() {
        return sizes;
    }

    public void setSizes(List<Size> sizes) {
        this.sizes = sizes;
    }
}
