package com.raycloud.sdk.shopee.domain;

/**
 * Created by donar on 17/6/12.
 */
public class Size {
    private Integer sizeId;
    private String name;
    private Integer defaultPrice;

    public Integer getSizeId() {
        return sizeId;
    }

    public void setSizeId(Integer sizeId) {
        this.sizeId = sizeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDefaultPrice() {
        return defaultPrice;
    }

    public void setDefaultPrice(Integer defaultPrice) {
        this.defaultPrice = defaultPrice;
    }
}
