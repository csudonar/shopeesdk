package com.raycloud.sdk.shopee.request.vo;

import com.raycloud.sdk.shopee.constant.ApiField;
import com.raycloud.sdk.shopee.constant.MustField;

/**
 * Created by donar on 17/6/12.
 */
public class Attribute {
    @ApiField("attributes_id") @MustField private Integer attributeId;
    @ApiField("value") @MustField private String value;
    public Integer getAttributeId() {
        return attributeId;
    }
    public void setAttributeId(Integer attributeId) {
        this.attributeId = attributeId;
    }
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
