package com.raycloud.sdk.shopee.request;

import com.raycloud.sdk.shopee.constant.ApiField;
import com.raycloud.sdk.shopee.constant.MustField;
import com.raycloud.sdk.shopee.exception.ApiRuleException;
import com.raycloud.sdk.shopee.response.ItemAddResponse;
import com.raycloud.sdk.shopee.response.ItemGetAttributesResponse;
import com.raycloud.sdk.shopee.response.ItemUpdatePriceResponse;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Created by donar on 17/6/12.
 */
public class ItemUpdatePriceRequest extends BaseShopeeRequest implements ShopeeRequest<ItemUpdatePriceResponse> {
    @MustField @ApiField("item_id") private Long itemId;
    @MustField @ApiField("price") private Float price;
    public ItemUpdatePriceRequest(Integer shopId, Integer patenerId, String secret){
        super(shopId,patenerId,secret);
    }

    public Long getItemId() {
        return itemId;
    }

    public ItemUpdatePriceRequest setItemId(Long itemId) {
        this.itemId = itemId;
        return this;
    }

    public Float getPrice() {
        return price;
    }

    public ItemUpdatePriceRequest setPrice(Float price) {
        this.price = price;
        return this;
    }

    public String getApiMethodName() {
        return "/items/update_price";
    }

    public Map<String, Object> getParams() throws ApiRuleException {
        try {
            Map<String,Object> map = getRequestMap(this);
            map.put("partner_id",getPartnerId());
            map.put("shopid",getShopId());
            map.put("timestamp",getTimestamp());
            return map;
        } catch (InvocationTargetException e) {
           throw new RuntimeException("sdk error");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("sdk error");
        } catch (ApiRuleException e) {
            throw e;
        }
    }

    public Class getResponseClass() {
        return ItemUpdatePriceResponse.class;
    }

    public void check() throws ApiRuleException {

    }

}
