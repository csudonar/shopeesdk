package com.raycloud.sdk.shopee.request;

import com.raycloud.sdk.shopee.constant.ApiField;
import com.raycloud.sdk.shopee.constant.MustField;
import com.raycloud.sdk.shopee.exception.ApiRuleException;
import com.raycloud.sdk.shopee.response.ItemAddResponse;
import com.raycloud.sdk.shopee.response.ItemGetAttributesResponse;
import com.raycloud.sdk.shopee.response.ItemGetItemsListResponse;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Created by donar on 17/6/12.
 */
public class ItemGetItemsListRequest extends BaseShopeeRequest implements ShopeeRequest<ItemGetItemsListResponse> {
    @MustField @ApiField("pagination_offset") private Integer paginationOffset;
    @MustField @ApiField("pagination_entries_per_page") private Integer paginationEntriesPerPage;
    public ItemGetItemsListRequest(Integer shopId, Integer patenerId, String secret){
        super(shopId,patenerId,secret);
    }

    public Integer getPaginationOffset() {
        return paginationOffset;
    }

    public ItemGetItemsListRequest setPaginationOffset(Integer paginationOffset) {
        this.paginationOffset = paginationOffset;
        return this;
    }

    public Integer getPaginationEntriesPerPage() {
        return paginationEntriesPerPage;
    }

    public ItemGetItemsListRequest setPaginationEntriesPerPage(Integer paginationEntriesPerPage) {
        this.paginationEntriesPerPage = paginationEntriesPerPage;
        return this;
    }

    public String getApiMethodName() {
        return "/items/get";
    }

    public Map<String, Object> getParams() throws ApiRuleException {
        try {
            Map<String,Object> map = getRequestMap(this);
            map.put("partner_id",getPartnerId());
            map.put("shopid",getShopId());
            map.put("timestamp",getTimestamp());
            return map;
        } catch (InvocationTargetException e) {
           throw new RuntimeException("sdk error");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("sdk error");
        } catch (ApiRuleException e) {
            throw e;
        }
    }

    public Class getResponseClass() {
        return ItemGetItemsListResponse.class;
    }

    public void check() throws ApiRuleException {

    }

}
