package com.raycloud.sdk.shopee.request.vo;

import com.raycloud.sdk.shopee.constant.ApiField;
import com.raycloud.sdk.shopee.constant.MustField;

/**
 * Created by donar on 17/6/12.
 */
public class Logistic {
    @MustField @ApiField("logistic_id") private Integer logisticId;
    @MustField @ApiField("enabled") private Boolean enabled;
    @ApiField("shipping_fee") private Float shoppingFee;
    @ApiField("size_id") private Integer sizeId;
    @ApiField("is_free") private Boolean isFree;
}
