package com.raycloud.sdk.shopee.request;

import com.raycloud.sdk.shopee.constant.ApiField;
import com.raycloud.sdk.shopee.constant.MustField;
import com.raycloud.sdk.shopee.exception.ApiRuleException;
import com.raycloud.sdk.shopee.response.ItemUpdateVariationPriceResponse;
import com.raycloud.sdk.shopee.response.ItemUpdateVariationStockResponse;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Created by donar on 17/6/12.
 */
public class ItemUpdateVariationStockRequest extends BaseShopeeRequest implements ShopeeRequest<ItemUpdateVariationStockResponse> {
    @MustField @ApiField("item_id") private Long itemId;
    @MustField @ApiField("stock") private Integer stock;
    @ApiField("variation_id") private Long variationId;
    public ItemUpdateVariationStockRequest(Integer shopId, Integer patenerId, String secret){
        super(shopId,patenerId,secret);
    }

    public Long getVariationId() {
        return variationId;
    }

    public ItemUpdateVariationStockRequest setVariationId(Long variationId) {
        this.variationId = variationId;
        return this;
    }

    public Long getItemId() {
        return itemId;
    }

    public ItemUpdateVariationStockRequest setItemId(Long itemId) {
        this.itemId = itemId;
        return this;
    }

    public Integer getStock() {
        return stock;
    }

    public ItemUpdateVariationStockRequest setStock(Integer stock) {
        this.stock = stock;
        return this;
    }

    public String getApiMethodName() {
        return "/items/update_variation_stock";
    }

    public Map<String, Object> getParams() throws ApiRuleException {
        try {
            Map<String,Object> map = getRequestMap(this);
            map.put("partner_id",getPartnerId());
            map.put("shopid",getShopId());
            map.put("timestamp",getTimestamp());
            return map;
        } catch (InvocationTargetException e) {
           throw new RuntimeException("sdk error");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("sdk error");
        } catch (ApiRuleException e) {
            throw e;
        }
    }

    public Class getResponseClass() {
        return ItemUpdateVariationPriceResponse.class;
    }

    public void check() throws ApiRuleException {

    }

}
