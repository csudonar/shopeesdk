package com.raycloud.sdk.shopee.request;

import com.raycloud.sdk.shopee.exception.ApiRuleException;
import com.raycloud.sdk.shopee.response.ShopeeResponse;

import java.util.Map;

public interface ShopeeRequest<T extends ShopeeResponse> {

	public Long getTimestamp();

	public String getSecretKey();

	public String getApiMethodName();

	public Map<String, Object> getParams() throws ApiRuleException;

	public Class<T> getResponseClass();

	/**
	 * 客户端参数检查，减少服务端无效调用
	 */
	public void check() throws ApiRuleException;

}
