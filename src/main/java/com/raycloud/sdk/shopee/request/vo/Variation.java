package com.raycloud.sdk.shopee.request.vo;

import com.raycloud.sdk.shopee.constant.ApiField;
import com.raycloud.sdk.shopee.constant.MustField;

/**
 * Created by donar on 17/6/12.
 */
public class Variation {
    @MustField @ApiField("name") private String name;
    @MustField @ApiField("stock") private Integer stock;
    @MustField @ApiField("price") private Float price;
    @MustField @ApiField("variation_sku") private String variationSku;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getVariationSku() {
        return variationSku;
    }

    public void setVariationSku(String variationSku) {
        this.variationSku = variationSku;
    }
}
