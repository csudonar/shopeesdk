package com.raycloud.sdk.shopee.request;

import com.raycloud.sdk.shopee.constant.ApiField;
import com.raycloud.sdk.shopee.constant.MustField;
import com.raycloud.sdk.shopee.exception.ApiRuleException;
import com.raycloud.sdk.shopee.response.ItemUpdateStockResponse;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Created by donar on 17/6/12.
 */
public class ItemUpdateStockRequest extends BaseShopeeRequest implements ShopeeRequest<ItemUpdateStockResponse> {
    @MustField @ApiField("item_id") private Long itemId;
    @MustField @ApiField("stock") private Integer stock;
    public ItemUpdateStockRequest(Integer shopId, Integer patenerId, String secret){
        super(shopId,patenerId,secret);
    }

    public Long getItemId() {
        return itemId;
    }

    public ItemUpdateStockRequest setItemId(Long itemId) {
        this.itemId = itemId;
        return this;
    }

    public Integer getStock() {
        return stock;
    }

    public ItemUpdateStockRequest setStock(Integer stock) {
        this.stock = stock;
        return this;
    }

    public String getApiMethodName() {
        return "/items/update_stock";
    }

    public Map<String, Object> getParams() throws ApiRuleException {
        try {
            Map<String,Object> map = getRequestMap(this);
            map.put("partner_id",getPartnerId());
            map.put("shopid",getShopId());
            map.put("timestamp",getTimestamp());
            return map;
        } catch (InvocationTargetException e) {
           throw new RuntimeException("sdk error");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("sdk error");
        } catch (ApiRuleException e) {
            throw e;
        }
    }

    public Class getResponseClass() {
        return ItemUpdateStockResponse.class;
    }

    public void check() throws ApiRuleException {

    }

}
