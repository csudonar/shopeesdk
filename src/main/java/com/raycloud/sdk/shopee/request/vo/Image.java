package com.raycloud.sdk.shopee.request.vo;

import com.raycloud.sdk.shopee.constant.ApiField;
import com.raycloud.sdk.shopee.constant.MustField;

/**
 * Created by donar on 17/6/12.
 */
public class Image {
    @MustField @ApiField("url") private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
