package com.raycloud.sdk.shopee.request;

import com.raycloud.sdk.shopee.constant.ApiField;
import com.raycloud.sdk.shopee.constant.MustField;
import com.raycloud.sdk.shopee.request.vo.Attribute;
import com.raycloud.sdk.shopee.request.vo.Image;
import com.raycloud.sdk.shopee.request.vo.Logistic;
import com.raycloud.sdk.shopee.request.vo.Variation;
import com.raycloud.sdk.shopee.exception.ApiRuleException;
import com.raycloud.sdk.shopee.response.ItemAddResponse;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by donar on 17/6/12.
 */
public class ItemAddRequest extends BaseShopeeRequest implements ShopeeRequest<ItemAddResponse> {
    @MustField @ApiField("catagory_id") private Integer catagoryId;
    @MustField @ApiField("name") private String name;
    @MustField @ApiField("description") private String description;
    @MustField @ApiField("price") private Float price;
    @MustField @ApiField("stock") private Integer stock;
    @ApiField("item_sku") private String itemSku;
    @ApiField("variations") private List<Variation> variations;
    @MustField @ApiField("images") private List<Image> images;
    @MustField @ApiField("attributes") private List<Attribute> attributes;
    @MustField @ApiField("logistics") private List<Logistic> logistics;
    @ApiField("weight") private Float weight;
    @ApiField("package_length") private Float packageLength;
    @ApiField("package_width") private Float packageWidth;
    @ApiField("package_height") private Float packageHeight;
    public ItemAddRequest(Integer shopId,Integer patenerId,String secret){
        super(shopId,patenerId,secret);
    }
    public ItemAddRequest addVarition(Variation variation) {
        if (variations == null) variations = new ArrayList<Variation>();
        variations.add(variation);
        return this;
    }
    public ItemAddRequest addImage(Image image) {
        if (images == null) images = new ArrayList<Image>();
        images.add(image);
        return this;
    }
    public ItemAddRequest addAttribute(Attribute attribute) {
        if (attributes == null) attributes = new ArrayList<Attribute>();
        attributes.add(attribute);
        return this;
    }
    public ItemAddRequest addLogistic(Logistic logistic) {
        if (logistics == null) logistics = new ArrayList<Logistic>();
        logistics.add(logistic);
        return this;
    }

    public List<Variation> getVariations() {
        return variations;
    }

    public List<Image> getImages() {
        return images;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public List<Logistic> getLogistics() {
        return logistics;
    }

    public Integer getCatagoryId() {
        return catagoryId;
    }

    public ItemAddRequest setCatagoryId(Integer catagoryId) {
        this.catagoryId = catagoryId;
        return this;
    }

    public String getName() {
        return name;
    }

    public ItemAddRequest setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ItemAddRequest setDescription(String description) {
        this.description = description;
        return this;
    }

    public Float getPrice() {
        return price;
    }

    public ItemAddRequest setPrice(Float price) {
        this.price = price;
        return this;
    }

    public Integer getStock() {
        return stock;
    }

    public ItemAddRequest setStock(Integer stock) {
        this.stock = stock;
        return this;
    }

    public String getItemSku() {
        return itemSku;
    }

    public ItemAddRequest setItemSku(String itemSku) {
        this.itemSku = itemSku;
        return this;
    }

    public Float getWeight() {
        return weight;
    }

    public ItemAddRequest setWeight(Float weight) {
        this.weight = weight;
        return this;
    }

    public Float getPackageLength() {
        return packageLength;
    }

    public ItemAddRequest setPackageLength(Float packageLength) {
        this.packageLength = packageLength;
        return this;
    }

    public Float getPackageWidth() {
        return packageWidth;
    }

    public ItemAddRequest setPackageWidth(Float packageWidth) {
        this.packageWidth = packageWidth;
        return this;
    }

    public Float getPackageHeight() {
        return packageHeight;
    }

    public ItemAddRequest setPackageHeight(Float packageHeight) {
        this.packageHeight = packageHeight;
        return this;
    }

    public String getApiMethodName() {
        return "/item/add";
    }

    public Map<String, Object> getParams() throws ApiRuleException {
        try {
            Map<String,Object> map = getRequestMap(this);
            map.put("partner_id",getPartnerId());
            map.put("shopid",getShopId());
            map.put("timestamp",getTimestamp());
            return map;
        } catch (InvocationTargetException e) {
           throw new RuntimeException("sdk error");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("sdk error");
        } catch (ApiRuleException e) {
            throw e;
        }
    }

    public Class getResponseClass() {
        return ItemAddResponse.class;
    }

    public void check() throws ApiRuleException {

    }

}
