package com.raycloud.sdk.shopee.request;

import com.raycloud.sdk.shopee.constant.ApiField;
import com.raycloud.sdk.shopee.constant.MustField;
import com.raycloud.sdk.shopee.exception.ApiRuleException;
import com.raycloud.sdk.shopee.response.ItemAddResponse;
import com.raycloud.sdk.shopee.response.ItemUpdatePriceResponse;
import com.raycloud.sdk.shopee.response.ItemUpdateVariationPriceResponse;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Created by donar on 17/6/12.
 */
public class ItemUpdateVariationPriceRequest extends BaseShopeeRequest implements ShopeeRequest<ItemUpdateVariationPriceResponse> {
    @MustField @ApiField("item_id") private Long itemId;
    @ApiField("variation_id") private Long variationId;
    @MustField @ApiField("price") private Float price;
    public ItemUpdateVariationPriceRequest(Integer shopId, Integer patenerId, String secret){
        super(shopId,patenerId,secret);
    }

    public Long getItemId() {
        return itemId;
    }

    public ItemUpdateVariationPriceRequest setItemId(Long itemId) {
        this.itemId = itemId;
        return this;
    }

    public Long getVariationId() {
        return variationId;
    }

    public ItemUpdateVariationPriceRequest setVariationId(Long variationId) {
        this.variationId = variationId;
        return this;
    }

    public Float getPrice() {
        return price;
    }

    public ItemUpdateVariationPriceRequest setPrice(Float price) {
        this.price = price;
        return this;
    }

    public String getApiMethodName() {
        return "/items/update_variation_price";
    }

    public Map<String, Object> getParams() throws ApiRuleException {
        try {
            Map<String,Object> map = getRequestMap(this);
            map.put("partner_id",getPartnerId());
            map.put("shopid",getShopId());
            map.put("timestamp",getTimestamp());
            return map;
        } catch (InvocationTargetException e) {
           throw new RuntimeException("sdk error");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("sdk error");
        } catch (ApiRuleException e) {
            throw e;
        }
    }

    public Class getResponseClass() {
        return ItemUpdateVariationPriceResponse.class;
    }

    public void check() throws ApiRuleException {

    }

}
