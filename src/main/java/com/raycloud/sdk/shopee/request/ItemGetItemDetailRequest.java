package com.raycloud.sdk.shopee.request;

import com.raycloud.sdk.shopee.constant.ApiField;
import com.raycloud.sdk.shopee.constant.MustField;
import com.raycloud.sdk.shopee.exception.ApiRuleException;
import com.raycloud.sdk.shopee.response.ItemAddResponse;
import com.raycloud.sdk.shopee.response.ItemGetCategoriesResponse;
import com.raycloud.sdk.shopee.response.ItemGetItemDetailResponse;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Created by donar on 17/6/12.
 */
public class ItemGetItemDetailRequest extends BaseShopeeRequest implements ShopeeRequest<ItemGetItemDetailResponse> {
    @ApiField("item_id")@MustField private Long itemId;
    public ItemGetItemDetailRequest(Integer shopId, Integer patenerId, String secret){
        super(shopId,patenerId,secret);
    }
    public String getApiMethodName() {
        return "/item/get";
    }

    public Map<String, Object> getParams() throws ApiRuleException {
        try {
            Map<String,Object> map = getRequestMap(this);
            map.put("partner_id",getPartnerId());
            map.put("shopid",getShopId());
            map.put("timestamp",getTimestamp());
            return map;
        } catch (InvocationTargetException e) {
           throw new RuntimeException("sdk error");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("sdk error");
        } catch (ApiRuleException e) {
            throw e;
        }
    }

    public Class getResponseClass() {
        return ItemGetItemDetailResponse.class;
    }

    public void check() throws ApiRuleException {

    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }
}
