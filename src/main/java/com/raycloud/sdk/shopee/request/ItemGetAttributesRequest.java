package com.raycloud.sdk.shopee.request;

import com.raycloud.sdk.shopee.constant.ApiField;
import com.raycloud.sdk.shopee.constant.MustField;
import com.raycloud.sdk.shopee.exception.ApiRuleException;
import com.raycloud.sdk.shopee.response.ItemGetAttributesResponse;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Created by donar on 17/6/12.
 */
public class ItemGetAttributesRequest extends BaseShopeeRequest implements ShopeeRequest<ItemGetAttributesResponse> {
    @MustField @ApiField("category_id") private Integer catagoryId;
    public ItemGetAttributesRequest(Integer shopId, Integer patenerId, String secret){
        super(shopId,patenerId,secret);
    }

    public Integer getCatagoryId() {
        return catagoryId;
    }

    public ItemGetAttributesRequest setCatagoryId(Integer catagoryId) {
        this.catagoryId = catagoryId;
        return this;
    }

    public String getApiMethodName() {
        return "/item/attributes/get";
    }

    public Map<String, Object> getParams() throws ApiRuleException {
        try {
            Map<String,Object> map = getRequestMap(this);
            map.put("partner_id",getPartnerId());
            map.put("shopid",getShopId());
            map.put("timestamp",getTimestamp());
            return map;
        } catch (InvocationTargetException e) {
           throw new RuntimeException("sdk error");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("sdk error");
        } catch (ApiRuleException e) {
            throw e;
        }
    }

    public Class getResponseClass() {
        return ItemGetAttributesResponse.class;
    }

    public void check() throws ApiRuleException {

    }

}
